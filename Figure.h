#pragma once
#include <iostream>
#include <windows.h>
#include <vector>

class Figure
{
public:
	std::vector<int> Dots;
	virtual void Draw(HDC hdc, HPEN hPen) {};
};

