#pragma once
#include "Open.h"
class Straight :
    public Open
{
public:
	virtual void Draw(HDC hdc, HPEN hPen) override {
		SelectObject(hdc, hPen);
		MoveToEx(hdc, Dots[0], Dots[1], NULL);
		LineTo(hdc, Dots[2], Dots[3]);
		Dots.clear();
	}
};

