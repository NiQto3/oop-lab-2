#include <ctime>
#include <iostream>
#include "DrawBack.h"
#include <windows.h>
#include <wingdi.h>

//Draw background
void DrawBackgrownd(HDC hdc) {
	RECT r1{ }, r2{ };
	r1 = { 1670, 0, 1920, 1080 };
	FillRect(hdc, &r1, (HBRUSH)CreateSolidBrush(RGB(240, 240, 240)));
	r1 = { 1665, 0, 1675, 1080 };
	FillRect(hdc, &r1, (HBRUSH)CreateSolidBrush(RGB(0, 0, 0)));
	r1 = { 1700, 700, 1720, 720 };
	FillRect(hdc, &r1, (HBRUSH)CreateSolidBrush(RGB(0, 0, 0)));
	r1 = { 1695, 690, 1850, 780 };
	FillRect(hdc, &r1, (HBRUSH)CreateSolidBrush(RGB(255, 255, 255)));
	TextOut(hdc, 1700, 695, L"Secelcted color", 15);
	r1 = { 1810, 695, 1830, 715 };// cube of selected color
	FillRect(hdc, &r1, (HBRUSH)CreateSolidBrush(RGB(0, 0, 0)));

	r1 = { 1700, 720, 1720, 740 };
	FillRect(hdc, &r1, (HBRUSH)CreateSolidBrush(RGB(0, 0, 0)));
	r1 = { 1730, 720, 1750, 740 };
	FillRect(hdc, &r1, (HBRUSH)CreateSolidBrush(RGB(105, 37, 37)));
	r1 = { 1760, 720, 1780, 740 };
	FillRect(hdc, &r1, (HBRUSH)CreateSolidBrush(RGB(255, 0, 0)));
	r1 = { 1790, 720, 1810, 740 };
	FillRect(hdc, &r1, (HBRUSH)CreateSolidBrush(RGB(255, 124, 10)));
	r1 = { 1820, 720, 1840, 740 };
	FillRect(hdc, &r1, (HBRUSH)CreateSolidBrush(RGB(255, 255, 0)));

	r1 = { 1700, 750, 1720, 770 };
	FillRect(hdc, &r1, (HBRUSH)CreateSolidBrush(RGB(0, 255, 0)));
	r1 = { 1730, 750, 1750, 770 };
	FillRect(hdc, &r1, (HBRUSH)CreateSolidBrush(RGB(5, 152, 250)));
	r1 = { 1760, 750, 1780, 770 };
	FillRect(hdc, &r1, (HBRUSH)CreateSolidBrush(RGB(0, 64, 255)));
	r1 = { 1790, 750, 1810, 770 };
	FillRect(hdc, &r1, (HBRUSH)CreateSolidBrush(RGB(238, 0, 255)));
	r1 = { 1820, 750, 1840, 770 };
	FillRect(hdc, &r1, (HBRUSH)CreateSolidBrush(RGB(222, 222, 222)));
}

void ClearWindow(HDC hdc) {
	RECT rr = { 0, 0, 1665, 1080 };
	FillRect(hdc, &rr, (HBRUSH)CreateSolidBrush(RGB(255, 255, 255)));
}

void SelectColorR(HDC hdc, HBRUSH hBr) {
	RECT r1 = { 1810, 695, 1830, 715 };// cube of selected color
	FillRect(hdc, &r1, hBr);
}