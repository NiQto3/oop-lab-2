﻿// OOPlab1.cpp : Определяет точку входа для приложения.
//

#include "framework.h"
#include "OOPlab1.h"
#include <windowsx.h>
#include "DrawBack.h"
#include <iostream>
#include <windows.h>
#include <vector>
#include "Line.h"
#include "Elipse.h"
#include "Circ.h"
#include "Polygonn.h"
#include "Square.h"
#include "Rect.h"
#include "Triangle.h"

#define MAX_LOADSTRING 100

// Глобальные переменные:
HINSTANCE hInst;                                // текущий экземпляр
WCHAR szTitle[MAX_LOADSTRING];                  // Текст строки заголовка
WCHAR szWindowClass[MAX_LOADSTRING];            // имя класса главного окна

// Отправить объявления функций, включенных в этот модуль кода:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Разместите код здесь.

    // Инициализация глобальных строк
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_OOPLAB1, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Выполнить инициализацию приложения:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_OOPLAB1));

    MSG msg;

    // Цикл основного сообщения:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  ФУНКЦИЯ: MyRegisterClass()
//
//  ЦЕЛЬ: Регистрирует класс окна.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_OOPLAB1));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = 0;
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   ФУНКЦИЯ: InitInstance(HINSTANCE, int)
//
//   ЦЕЛЬ: Сохраняет маркер экземпляра и создает главное окно
//
//   КОММЕНТАРИИ:
//
//        В этой функции маркер экземпляра сохраняется в глобальной переменной, а также
//        создается и выводится главное окно программы.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Сохранить маркер экземпляра в глобальной переменной

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_POPUP,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, SW_SHOWMAXIMIZED);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  ФУНКЦИЯ: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  ЦЕЛЬ: Обрабатывает сообщения в главном окне.
//
//  WM_COMMAND  - обработать меню приложения
//  WM_PAINT    - Отрисовка главного окна
//  WM_DESTROY  - отправить сообщение о выходе и вернуться
//
//
HWND hDrawButton;
RECT r{ 0, 0, 1695, 1080 };
std::vector <int>buff;
int DotReq = 0, CurrDot = 0;
HPEN hPen = CreatePen(PS_SOLID, 0, RGB(0, 0, 0));;
HBRUSH hBr = CreateSolidBrush(RGB(0, 0, 0));

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_CREATE:
    {
        //Quit button
        HWND hQButton = CreateWindow(
            L"Button",
            L"Exit",
            WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
            1700, 1000, 200, 50, hWnd, reinterpret_cast<HMENU>(1001), nullptr, nullptr
        );

        //Draw polygon
        hDrawButton = CreateWindow(
            L"Button",
            L"Draw polygon",
            WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_DISABLED,
            1700, 260, 200, 50, hWnd, reinterpret_cast<HMENU>(1009), nullptr, nullptr
        );

        //Clear window
        HWND hClearButton = CreateWindow(
            L"Button",
            L"Clear sheet",
            WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
            1700, 330, 200, 50, hWnd, reinterpret_cast<HMENU>(1010), nullptr, nullptr
        );

        //Radiobutton group
        HWND hPoly = CreateWindow(
            L"Button", 
            L"Polygon",
            WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON,
            1760, 10, 90, 30, hWnd, reinterpret_cast<HMENU>(1002), nullptr, nullptr
        );
        HWND hTriangle = CreateWindow(
            L"Button",
            L"Triangle",
            WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON,
            1760, 40, 90, 30, hWnd, reinterpret_cast<HMENU>(1003), nullptr, nullptr
        );
        HWND hRectangle = CreateWindow(
            L"Button",
            L"Rectangle",
            WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON,
            1760, 70, 90, 30, hWnd, reinterpret_cast<HMENU>(1004), nullptr, nullptr
        );
        HWND hSquare= CreateWindow(
            L"Button",
            L"Square",
            WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON,
            1760, 100, 90, 30, hWnd, reinterpret_cast<HMENU>(1005), nullptr, nullptr
        );
        HWND hEllipse = CreateWindow(
            L"Button",
            L"Ellipse",
            WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON,
            1760, 130, 90, 30, hWnd, reinterpret_cast<HMENU>(1006), nullptr, nullptr
        );
        HWND hCircle = CreateWindow(
            L"Button",
            L"Circle",
            WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON,
            1760, 160, 90, 30, hWnd, reinterpret_cast<HMENU>(1007), nullptr, nullptr
        );
        HWND hLine = CreateWindow(
            L"Button",
            L"Line",
            WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON,
            1760, 190, 90, 30, hWnd, reinterpret_cast<HMENU>(1008), nullptr, nullptr
        );
    }

    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Разобрать выбор в меню:
            switch (wmId)
            {
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            case 1001:
                PostQuitMessage(0);
                break;
            case 1002:
                EnableWindow(hDrawButton, TRUE);
                DotReq = -1;
                CurrDot = 0;
                break;
            case 1003:
                EnableWindow(hDrawButton, FALSE);
                DotReq = 3;
                CurrDot = 0;
                break;
            case 1004:
                EnableWindow(hDrawButton, FALSE);
                DotReq = 2;
                CurrDot = 0;
                break;
            case 1005:
                EnableWindow(hDrawButton, FALSE);
                DotReq = 2;
                CurrDot = 0;
                break;
            case 1006:
                EnableWindow(hDrawButton, FALSE);
                DotReq = 2;
                CurrDot = 0;
                break;
            case 1007:
                EnableWindow(hDrawButton, FALSE);
                DotReq = 2;
                CurrDot = 0;
                break;
            case 1008:
                EnableWindow(hDrawButton, FALSE);
                DotReq = 2;
                CurrDot = 0;
                break;
            case 1009:
                CurrDot = -1;            
                InvalidateRect(hWnd, &r, FALSE);
                break;
            case 1010:
                DotReq = -2;
                InvalidateRect(hWnd, &r, FALSE);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Добавьте сюда любой код прорисовки, использующий HDC...           
            DrawBackgrownd(hdc);
            if (DotReq == -1) 
            {
                Polygonn TDraw(buff);
                TDraw.Draw(hdc, hPen);
            }
            if (DotReq == 2)
            {
                if (IsDlgButtonChecked(hWnd, 1006))
                {
                    Elipse TDraw(buff);
                    TDraw.Draw(hdc, hPen);
                }
                if (IsDlgButtonChecked(hWnd, 1007))
                {
                    Circ TDraw(buff);
                    TDraw.Draw(hdc, hPen);
                }
                if (IsDlgButtonChecked(hWnd, 1008))
                {
                    Line TDraw(buff);
                    TDraw.Draw(hdc, hPen);
                }
                if (IsDlgButtonChecked(hWnd, 1004))
                {
                    Rect TDraw(buff);
                    TDraw.Draw(hdc, hPen);
                }
                if (IsDlgButtonChecked(hWnd, 1005))
                {
                    Square TDraw(buff);
                    TDraw.Draw(hdc, hPen);
                }
            }
            if (DotReq == 3)
                {
                    Triangle TDraw(buff);
                    TDraw.Draw(hdc, hPen);
                }
            if (DotReq == -2)
                ClearWindow(hdc);
            buff.clear();
            SelectColorR(hdc, hBr);
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;

    case WM_LBUTTONDOWN:
        {
            if (wParam == VK_LBUTTON) {
                POINT dot;                
                GetCursorPos(&dot);
                if (dot.x < 1665)
                {
                    buff.push_back(dot.x);
                    buff.push_back(dot.y);
                    CurrDot++;
                    if (CurrDot == DotReq)
                    {
                        InvalidateRect(hWnd, &r, FALSE);
                        CurrDot = 0;
                    }
                } 
                if ((dot.x >= 1700) && (dot.x <= 1840) && (dot.y >= 720) && (dot.y <= 770))
                {
                    CurrDot = 0;
                    DotReq = 0;
                    buff.clear();
                    if (dot.y <= 740)
                    {
                        if (dot.x <= 1720)
                        {
                            hPen = CreatePen(PS_SOLID, 5, RGB(0, 0, 0));
                            hBr = CreateSolidBrush(RGB(0, 0, 0));
                            RECT tt = { 1695, 690, 1850, 780 };
                            InvalidateRect(hWnd, &tt, FALSE);
                            break;
                        }
                        if (dot.x <= 1750)
                        {
                            hPen = CreatePen(PS_SOLID, 5, RGB(105, 37, 37));
                            hBr = CreateSolidBrush(RGB(105, 37, 37));
                            RECT tt = { 1695, 690, 1850, 780 };
                            InvalidateRect(hWnd, &tt, FALSE);
                            break;
                        }
                        if (dot.x <= 1780)
                        {
                            hPen = CreatePen(PS_SOLID, 5, RGB(255, 0, 0));
                            hBr = CreateSolidBrush(RGB(255, 0, 0));
                            RECT tt = { 1695, 690, 1850, 780 };
                            InvalidateRect(hWnd, &tt, FALSE);
                            break;
                        }
                        if (dot.x <= 1810)
                        {
                            hPen = CreatePen(PS_SOLID, 5, RGB(255, 124, 10));
                            hBr = CreateSolidBrush(RGB(255, 124, 10));
                            RECT tt = { 1695, 690, 1850, 780 };
                            InvalidateRect(hWnd, &tt, FALSE);
                            break;
                        }
                        if (dot.x <= 1840)
                        {
                            hPen = CreatePen(PS_SOLID, 5, RGB(255, 255, 0));
                            hBr = CreateSolidBrush(RGB(255, 255, 0));
                            RECT tt = { 1695, 690, 1850, 780 };
                            InvalidateRect(hWnd, &tt, FALSE);
                            break;
                        }
                    }
                    if (dot.y >= 750)
                    {
                        if (dot.x <= 1720)
                        {
                            hPen = CreatePen(PS_SOLID, 5, RGB(0, 255, 0));
                            hBr = CreateSolidBrush(RGB(0, 255, 0));
                            RECT tt = { 1695, 690, 1850, 780 };
                            InvalidateRect(hWnd, &tt, FALSE);
                            break;
                        }
                        if (dot.x <= 1750)
                        {
                            hPen = CreatePen(PS_SOLID, 5, RGB(5, 152, 250));
                            hBr = CreateSolidBrush(RGB(5, 152, 250));
                            RECT tt = { 1695, 690, 1850, 780 };
                            InvalidateRect(hWnd, &tt, FALSE);
                            break;
                        }
                        if (dot.x <= 1780)
                        {
                            hPen = CreatePen(PS_SOLID, 5, RGB(0, 64, 255));
                            hBr = CreateSolidBrush(RGB(0, 64, 255));
                            RECT tt = { 1695, 690, 1850, 780 };
                            InvalidateRect(hWnd, &tt, FALSE);
                            break;
                        }
                        if (dot.x <= 1810)
                        {
                            hPen = CreatePen(PS_SOLID, 5, RGB(238, 0, 255));
                            hBr = CreateSolidBrush(RGB(238, 0, 255));
                            RECT tt = { 1695, 690, 1850, 780 };
                            InvalidateRect(hWnd, &tt, FALSE);
                            break;
                        }
                        if (dot.x <= 1840)
                        {
                            hPen = CreatePen(PS_SOLID, 5, RGB(222, 222, 222));
                            hBr = CreateSolidBrush(RGB(222, 222, 222));
                            RECT tt = { 1695, 690, 1850, 780 };
                            InvalidateRect(hWnd, &tt, FALSE);
                            break;
                        }
                    }
                }
                //hPen = CreatePen(PS_SOLID, 5, RGB(0, 0, 255));                                                
            }
        }

    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}
