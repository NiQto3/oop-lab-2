#pragma once
#include "Close.h"
class Hardf :
    public Close
{
public:
	virtual void Draw(HDC hdc, HPEN hPen) override {
		SelectObject(hdc, hPen);
		int a = 0;
		int b = 1;
		MoveToEx(hdc, Dots[a], Dots[b], NULL);
		while (b + 1 != size(Dots))
		{
			a += 2;
			b += 2;
			LineTo(hdc, Dots[a], Dots[b]);
		}
		LineTo(hdc, Dots[0], Dots[1]);
		Dots.clear();
	}
};

